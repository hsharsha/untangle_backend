# About
This is proof of concept of untangleAI backend. It serves web pages using nginx web server and also uses nginx as reverse proxy to make API calls into untangle backend.
Port 6500 is used to serve frontend.

# API
| Methods | Purpose |
| ------- | ------- |
|GET /historian/ | This mimics call to historian to get sensor data |
|POST /extrapolated | This sends historian data to untangle backend server to do linear extrapolation|
|POST /submitToLSTM | This submits extrapolated data to untangle backend to perform LSTM calculation and returns response of calculation

# Current Implementation
- Historian data is loaded from json file
- All the backend activities is done in python. C++ server delegates work to python which writes result back to stdout and server reads from stdout
- LSTM is simulated by doing  a random sleep of 1-10 s and returning data read from json file
- Assumed nginx is installed on the system
- Other dependencies like boost are downloaded

# How to run
- Edit nginx/nginx.conf to point to the correct front end directory
- ``` ./run.sh ``` To run the C++ backend server

# Limitations and TODO
- Passing json from C++ to python in the form of command line arguments looses json structure. Python fails to perform json.loads on that string
- Explore protobuf or serialization protocols to communicate between C++ and python
- Unit test and more
- Automate writing nginx configuraiton
- Tested on Ubuntu. Extend run.sh to other platforms
