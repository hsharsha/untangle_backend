let historian_data = null;

async function get_historian_data()
{
    const url = '/historian/';
    const response = await fetch(url);
    const val = await response.json();
    let ulist = document.getElementById("historian_data");
    historian_data = JSON.stringify(val);
    ulist.innerHTML = historian_data;
}

let extrapolated_data = null;
async function get_extrapolated_data()
{
    const url = '/extrapolated/';
    const response = await fetch(url,
        {method:'POST',
         body: historian_data,
         headers:{
            'Content-Type': 'application/json'
            }
        });
    const val = await response.json();
    let ulist = document.getElementById("extrapolated_data");
    extrapolated_data = JSON.stringify(val);
    ulist.innerHTML = extrapolated_data
}

async function submit_data_to_LSTM()
{
    const url = '/submitToLSTM';
    const response = await fetch(url,
        {method:'POST',
         body: extrapolated_data,
         headers:{
            'Content-Type': 'application/json'
            }
        });
    const val = await response.json();
    let ulist = document.getElementById("submit_data");
    ulist.innerHTML = JSON.stringify(val);
}
