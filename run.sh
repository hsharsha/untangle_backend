#!/bin/bash

echo "Please edit nginx/nginx.conf to point to right frontend directory"
read -p "Press any key to continue after editing..."
sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bkup
sudo cp nginx/nginx.conf /etc/nginx/
sudo service nginx restart
mkdir -p build
cd build && cmake .. && make -j4 && cd source && ./untangle_backend
