#pragma once

#include <string>

class Interactor
{
    std::string exec_system_cmd(const std::string&);
    public:
        std::string getHistorianData();
        std::string getExtrapolatedData(const std::string&);
        std::string submitDataToLSTM(const std::string&);
        std::string getResultFromLSTM();
};
