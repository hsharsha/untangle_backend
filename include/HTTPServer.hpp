#pragma once

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/strand.hpp>
#include <boost/config.hpp>
#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>


using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace http = boost::beast::http;   // from <boost/beast/http.hpp>

struct ServerReqParams
{
    http::verb method_;
    std::string target_;
    std::string body_;
};

struct ServerInitParams
{
    std::unordered_set<std::string> allowed_targets_;
    std::function<std::string(std::shared_ptr<ServerReqParams>)> handle_body_;
};

// This function produces an HTTP response for the given
// request. The type of the response object depends on the
// contents of the request, so the interface requires the
// caller to pass a generic lambda for receiving the response.
template<class Body, class Allocator, class Send>
void handle_request(http::request<Body, http::basic_fields<Allocator>>&& req, Send&& send,
                    std::shared_ptr<ServerInitParams> initParams)
{
    // Returns a bad request response
    auto const bad_request =
    [&req](boost::beast::string_view why)
    {
        http::response<http::string_body> res{http::status::bad_request, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = why.to_string();
        res.prepare_payload();
        return res;
    };

    // Returns a not found response
    auto const not_found =
    [&req](boost::beast::string_view target)
    {
        http::response<http::string_body> res{http::status::not_found, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = "The resource '" + target.to_string() + "' was not found.";
        res.prepare_payload();
        return res;
    };

    // Returns a server error response
    auto const server_error =
    [&req](boost::beast::string_view what)
    {
        http::response<http::string_body> res{http::status::internal_server_error, req.version()};
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = "An error occurred: '" + what.to_string() + "'";
        res.prepare_payload();
        return res;
    };

    if( req.target() == "/") {
        // Attempt to open the file
        boost::beast::error_code ec;
        http::file_body::value_type body;
        body.open("index.html", boost::beast::file_mode::scan, ec);

        // Handle the case where the file doesn't exist
        if(ec == boost::beast::errc::no_such_file_or_directory)
            return send(not_found(req.target()));

        // Handle an unknown error
        if(ec)
            return send(server_error(ec.message()));

        // Cache the size since we need it after the move
        auto const size = body.size();
        http::response<http::file_body> res{
            std::piecewise_construct,
                std::make_tuple(std::move(body)),
                std::make_tuple(http::status::ok, req.version())
        };
        res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
        res.set(http::field::content_type, "text/html");
        res.content_length(size);
        res.keep_alive(req.keep_alive());

        return send(std::move(res));
    }

    /*if(initParams->allowed_targets_.find(std::string(req.target())) == initParams->allowed_targets_.end()) {
        return send(not_found("target not found"));
    }*/

    std::string resp_body = "{}";
    try {
        auto servreq = std::make_shared<ServerReqParams>();
        servreq->method_ = req.method();
        servreq->target_ = std::string(req.target());
        servreq->body_ = std::string(req.body());
        resp_body = initParams->handle_body_(servreq);
    } catch(std::exception &ex) {
        std::cout << ex.what() << std::endl;
        return send(bad_request("Failed to parse json body"));
    }
    http::response<http::string_body> res{ http::status::ok, req.version()  };
    res.set(http::field::server, BOOST_BEAST_VERSION_STRING);
    res.set(http::field::content_type, "application/json");
    res.body() = resp_body;
    res.prepare_payload();
    res.keep_alive(req.keep_alive());
    return send(std::move(res));
}

//------------------------------------------------------------------------------

// Handles an HTTP server connection
class HTTPSession : public std::enable_shared_from_this<HTTPSession>
{
    // This is the C++11 equivalent of a generic lambda.
    // The function object is used to send an HTTP message.
    struct send_lambda
    {
        HTTPSession& self_;

        explicit send_lambda(HTTPSession& self) : self_(self) {}

        template<bool isRequest, class Body, class Fields>
        void operator()(http::message<isRequest, Body, Fields>&& msg) const
        {
            // The lifetime of the message has to extend
            // for the duration of the async operation so
            // we use a shared_ptr to manage it.
            auto sp = std::make_shared<
                http::message<isRequest, Body, Fields>>(std::move(msg));

            // Store a type-erased version of the shared
            // pointer in the class to keep it alive.
            self_.res_ = sp;

            // Write the response
            http::async_write(
                self_.socket_,
                *sp,
                boost::asio::bind_executor(
                    self_.strand_,
                    std::bind(
                        &HTTPSession::on_write,
                        self_.shared_from_this(),
                        std::placeholders::_1,
                        std::placeholders::_2,
                        sp->need_eof())));
        }
    };

    tcp::socket socket_;
    boost::asio::strand<boost::asio::io_context::executor_type> strand_;
    boost::beast::flat_buffer buffer_;
    http::request<http::string_body> req_;
    std::shared_ptr<void> res_;
    send_lambda lambda_;
    std::shared_ptr<ServerInitParams> initParams_;

    // Report a failure
    void fail(boost::system::error_code ec, char const* what)
    {
        std::cerr << what << ": " << ec.message() << "\n";
    }

    void do_read()
    {
        // Read a request
        http::async_read(socket_, buffer_, req_,
            boost::asio::bind_executor(
                strand_,
                std::bind(
                    &HTTPSession::on_read,
                    shared_from_this(),
                    std::placeholders::_1,
                    std::placeholders::_2)));
    }

    void on_read(boost::system::error_code ec, std::size_t bytes_transferred)
    {
        boost::ignore_unused(bytes_transferred);

        // This means they closed the connection
        if(ec == http::error::end_of_stream) {
            return do_close();
        }

        if(ec) {
            return fail(ec, "read");
        }

        // Send the response
        handle_request(std::move(req_), lambda_, initParams_);
    }

    void on_write(boost::system::error_code ec, std::size_t bytes_transferred, bool close)
    {
        boost::ignore_unused(bytes_transferred);

        if(ec) {
            return fail(ec, "write");
        }

        if(close){
            // This means we should close the connection, usually because
            // the response indicated the "Connection: close" semantic.
            return do_close();
        }

        // We're done with the response so delete it
        res_ = nullptr;

        // Read another request
        do_read();
    }

    void do_close()
    {
        // Send a TCP shutdown
        boost::system::error_code ec;
        socket_.shutdown(tcp::socket::shutdown_send, ec);

        // At this point the connection is closed gracefully
    }

    public:
        // Take ownership of the socket
        explicit HTTPSession(tcp::socket socket, std::shared_ptr<ServerInitParams> initParams)
            : socket_(std::move(socket)),
              strand_(socket_.get_executor()),
              lambda_(*this),
              initParams_(initParams) {}

        // Start the asynchronous operation
        void run()
        {
            do_read();
        }
};

//------------------------------------------------------------------------------

// Accepts incoming connections and launches the HTTPSessions
class HTTPListener : public std::enable_shared_from_this<HTTPListener>
{
    tcp::acceptor acceptor_;
    tcp::socket socket_;
    std::shared_ptr<ServerInitParams> initParams_;

    void do_accept()
    {
        acceptor_.async_accept(
            socket_,
            std::bind(
                &HTTPListener::on_accept,
                shared_from_this(),
                std::placeholders::_1));
    }

    // Report a failure
    void fail(boost::system::error_code ec, char const* what)
    {
        std::cerr << what << ": " << ec.message() << "\n";
    }

    void on_accept(boost::system::error_code ec)
    {
        if(ec) {
            fail(ec, "accept");
        } else {
            // Create the HTTPSession and run it
            std::make_shared<HTTPSession>(std::move(socket_), initParams_)->run();
        }
        // Accept another connection
        do_accept();
    }

    public:
        HTTPListener(boost::asio::io_context& ioc, tcp::endpoint endpoint,
                     std::shared_ptr<ServerInitParams> initParams)
            : acceptor_(ioc),
              socket_(ioc),
              initParams_(initParams)
        {
            boost::system::error_code ec;

            // Open the acceptor
            acceptor_.open(endpoint.protocol(), ec);
            if(ec) {
                fail(ec, "open");
                return;
            }

            // Bind to the server address
            acceptor_.bind(endpoint, ec);
            if(ec) {
                fail(ec, "bind");
                return;
            }

            // Start listening for connections
            acceptor_.listen(boost::asio::socket_base::max_listen_connections, ec);
            if(ec) {
                fail(ec, "listen");
                return;
            }
        }

        // Start accepting incoming connections
        void run()
        {
            if(! acceptor_.is_open()) {
                return;
            }
            do_accept();
        }
};


