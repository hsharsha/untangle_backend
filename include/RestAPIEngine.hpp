#pragma once

#include "HTTPServer.hpp"
#include "Interactor.hpp"


class RestAPIEngine
{
    boost::asio::io_context& ioc_;
    std::string host_;
    uint16_t port_;
    std::unordered_set<std::string> allowed_targets_;
    Interactor interactor;

    public:
        explicit RestAPIEngine(boost::asio::io_context&);
        void run(void);
        std::string handleServerReq(std::shared_ptr<ServerReqParams>);
};

