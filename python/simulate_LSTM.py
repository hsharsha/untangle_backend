#!/usr/bin/python

import json
import sys
import time
import random

with open('python/test_file.json', 'r') as histfile:
    data = json.load(histfile);
    time.sleep(random.randint(1,10)) # sleep anywhere between 1 to 10 seconds time taken by LSTM
    print json.dumps(data, indent=4)
