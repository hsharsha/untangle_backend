#!/usr/bin/python

import json
import sys
import datetime
import copy

with open('python/historian.json', 'r') as histfile:
    data = json.load(histfile);
    sorted_data = sorted(data, key=lambda k: k['Time'])
    extrapolated_data = []
    for i in range(len(sorted_data)-1):
        nxt = datetime.datetime.strptime(sorted_data[i+1]['Time'] , '%Y-%m-%dT%H:%M:%S.%fZ')
        cur = datetime.datetime.strptime(sorted_data[i]['Time'] , '%Y-%m-%dT%H:%M:%S.%fZ')
        difftime = (nxt-cur).total_seconds()/60.0
        diffval = int(sorted_data[i+1]['Value']) - int(sorted_data[i]['Value'])
        delta = diffval/difftime
        for j in range(int(difftime)):
            time = cur + datetime.timedelta(minutes=j)
            val = copy.copy(sorted_data[i])
            val['Time'] = time.isoformat();
            val['Value'] = sorted_data[i]['Value'] + j*delta
            extrapolated_data.append(val)

    # add the last item
    extrapolated_data.append(sorted_data[-1])
    print json.dumps(extrapolated_data, indent=4)
