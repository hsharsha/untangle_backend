#!/usr/bin/python

import json
import sys

with open(sys.argv[1], 'r') as histfile:
    data = json.load(histfile);
    print json.dumps(data, indent=4)
