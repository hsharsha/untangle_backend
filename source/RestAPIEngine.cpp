#include "RestAPIEngine.hpp"

RestAPIEngine::RestAPIEngine(boost::asio::io_context &ioc)
    : ioc_(ioc)
{
    // Start HTTP Server
    host_ = "0.0.0.0";
    port_ = 6000;
    std::cout << "Staring HTTP Server on host: " << host_ << " and port: " << port_ << std::endl;
    allowed_targets_ = {"/historian/", "/extrapolated/", "/resFromLSTM/", "/submitToLSTM/"};
}

std::string RestAPIEngine::handleServerReq(std::shared_ptr<ServerReqParams> reqParam)
{
    std::string respBody = "{}";
    if (allowed_targets_.find(reqParam->target_) == allowed_targets_.end()) {
        respBody = "Unsupported target";
    } else {
        if(reqParam->method_  == http::verb::get) {
            if(reqParam->target_ == std::string("/historian/")) {
                respBody = interactor.getHistorianData();
            } else if(reqParam->target_ == std::string("/resFromLSTM/")) {
                respBody = interactor.getResultFromLSTM();
            }
        } else if(reqParam->method_ == http::verb::post) {
            if(reqParam->target_ == std::string("/submitToLSTM/")) {
                respBody = interactor.submitDataToLSTM(reqParam->body_);
            } else if(reqParam->target_ == std::string("/extrapolated/")) {
                respBody = interactor.getExtrapolatedData(reqParam->body_);
            }
        } else {
            respBody = "Unsupported HTTP method";
        }
    }
    return respBody;
}

void RestAPIEngine::run(void)
{
    auto const address = boost::asio::ip::make_address(host_);
    auto initParams = std::make_shared<ServerInitParams>();
    initParams->allowed_targets_ = this->allowed_targets_;
    initParams->handle_body_ = std::bind(&RestAPIEngine::handleServerReq, this, std::placeholders::_1);
    std::make_shared<HTTPListener>(ioc_, tcp::endpoint{address, port_}, initParams)->run();
    return;
}
