#include "Interactor.hpp"
#include <iostream>
#include <array>
#include <memory>

std::string Interactor::exec_system_cmd(const std::string& cmd)
{
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()))
            result += buffer.data();
    }
    return result;
}

std::string Interactor::getHistorianData()
{
    auto str = exec_system_cmd("python ./python/get_historian.py ./python/historian.json");
    std::cout << str << std::endl;
    return str;
}

std::string Interactor::submitDataToLSTM(const std::string& extrapolate_data)
{

    std::string cmd = "python ./python/simulate_LSTM.py " + extrapolate_data;
    std::cout << cmd << std::endl;
    auto str = exec_system_cmd(cmd);
    std::cout << str << std::endl;
    return str;
}

std::string Interactor::getResultFromLSTM()
{
    return "";
}

std::string Interactor::getExtrapolatedData(const std::string& hist_data)
{
    std::cout << hist_data << std::endl;
    std::string cmd = "python ./python/extrapolate_data.py " + hist_data;
    std::cout << cmd << std::endl;
    auto str = exec_system_cmd(cmd);
    std::cout << str << std::endl;
    return str;
}
