#include "RestAPIEngine.hpp"

static const unsigned int CONCURRENCY = 1;

int main(void)
{
    int threads = std::min(std::thread::hardware_concurrency(), CONCURRENCY);
    // The io_context is required for all I/O
    boost::asio::io_context ioc{threads};

    // Create and launch a API RoutingEngine
    RestAPIEngine re = RestAPIEngine(ioc);
    re.run();

    // Run the I/O service on the requested number of threads
    std::vector<std::thread> v;
    v.reserve(threads - 1);
    for(auto i = threads - 1; i > 0; --i){
        v.emplace_back([&ioc]{ioc.run();});
    }
    ioc.run();

    return EXIT_SUCCESS;
}
